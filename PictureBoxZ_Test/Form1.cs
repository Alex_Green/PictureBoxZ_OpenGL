﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace PictureBoxZ_Test
{
    public partial class Form1 : Form
    {
        Bitmap bmp;

        public Form1()
        {
            InitializeComponent();
            pictureBoxZ1.Image = bmp = new Bitmap("Test.png");

            TextureMinFilter MinFilter = pictureBoxZ1.MinFilter;
            TextureMagFilter MagFilter = pictureBoxZ1.MagFilter;
            comboBoxMinFilter.DataSource = (TextureMinFilter[])Enum.GetValues(typeof(TextureMinFilter));
            comboBoxMagFilter.DataSource = (TextureMagFilter[])Enum.GetValues(typeof(TextureMagFilter));
            comboBoxMinFilter.SelectedItem = MinFilter;
            comboBoxMagFilter.SelectedItem = MagFilter;
            checkBoxMipMaps.Checked = pictureBoxZ1.MipMaps;
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                Close();
            else if (e.KeyCode == Keys.F1)
                pictureBoxZ1.Image = (pictureBoxZ1.Image == null ? bmp : null);
            else if (e.KeyCode == Keys.F2)
            {
                BackgroundImage = (pictureBoxZ1.Visible ? pictureBoxZ1.Image : null);
                pictureBoxZ1.Visible = !pictureBoxZ1.Visible;
            }
            else if (e.KeyCode == Keys.F3)
                panelSettings.Visible = !panelSettings.Visible;
        }

        private void pictureBoxZ1_MouseClick_Move(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && e.X >= 0 && e.Y >= 0 &&
                e.X < pictureBoxZ1.ClientRectangle.Width && e.Y < pictureBoxZ1.ClientRectangle.Height)
            {
                Color NMapColor = Color.FromArgb(pictureBoxZ1.GetColorAtPoint(e.Location).ToArgb());
                Text = String.Format("RGBA: [{0}; {1}; {2}; {3}]", NMapColor.R, NMapColor.G, NMapColor.B, NMapColor.A);
            }
        }

        private void comboBoxMinFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            pictureBoxZ1.MinFilter = (TextureMinFilter)comboBoxMinFilter.SelectedItem;
        }

        private void comboBoxMagFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            pictureBoxZ1.MagFilter = (TextureMagFilter)comboBoxMagFilter.SelectedItem;
        }

        private void checkBoxMipMaps_CheckedChanged(object sender, EventArgs e)
        {
            pictureBoxZ1.MipMaps = checkBoxMipMaps.Checked;
        }
    }
}
