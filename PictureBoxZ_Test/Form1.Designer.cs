﻿namespace PictureBoxZ_Test
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.pictureBoxZ1 = new PictureBoxZ.PictureBoxZ();
            this.panelSettings = new System.Windows.Forms.Panel();
            this.checkBoxMipMaps = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxMagFilter = new System.Windows.Forms.ComboBox();
            this.comboBoxMinFilter = new System.Windows.Forms.ComboBox();
            this.panelSettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBoxZ1
            // 
            this.pictureBoxZ1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.pictureBoxZ1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxZ1.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxZ1.MinFilter = OpenTK.Graphics.OpenGL.TextureMinFilter.Linear;
            this.pictureBoxZ1.Name = "pictureBoxZ1";
            this.pictureBoxZ1.Size = new System.Drawing.Size(284, 261);
            this.pictureBoxZ1.TabIndex = 0;
            this.pictureBoxZ1.VSync = true;
            this.pictureBoxZ1.ZoomCenterPoint = ((System.Drawing.PointF)(resources.GetObject("pictureBoxZ1.ZoomCenterPoint")));
            this.pictureBoxZ1.ZoomList = ((System.Collections.Generic.List<float>)(resources.GetObject("pictureBoxZ1.ZoomList")));
            this.pictureBoxZ1.ZoomListIndex = -1;
            this.pictureBoxZ1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBoxZ1_MouseClick_Move);
            this.pictureBoxZ1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxZ1_MouseClick_Move);
            // 
            // panelSettings
            // 
            this.panelSettings.BackColor = System.Drawing.SystemColors.Control;
            this.panelSettings.Controls.Add(this.checkBoxMipMaps);
            this.panelSettings.Controls.Add(this.label2);
            this.panelSettings.Controls.Add(this.label1);
            this.panelSettings.Controls.Add(this.comboBoxMagFilter);
            this.panelSettings.Controls.Add(this.comboBoxMinFilter);
            this.panelSettings.Location = new System.Drawing.Point(0, 0);
            this.panelSettings.Name = "panelSettings";
            this.panelSettings.Size = new System.Drawing.Size(246, 80);
            this.panelSettings.TabIndex = 1;
            this.panelSettings.Visible = false;
            // 
            // checkBoxMipMaps
            // 
            this.checkBoxMipMaps.AutoSize = true;
            this.checkBoxMipMaps.Location = new System.Drawing.Point(6, 56);
            this.checkBoxMipMaps.Name = "checkBoxMipMaps";
            this.checkBoxMipMaps.Size = new System.Drawing.Size(91, 17);
            this.checkBoxMipMaps.TabIndex = 4;
            this.checkBoxMipMaps.Text = "Use MipMaps";
            this.checkBoxMipMaps.UseVisualStyleBackColor = true;
            this.checkBoxMipMaps.CheckedChanged += new System.EventHandler(this.checkBoxMipMaps_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "MagFilter:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "MinFilter:";
            // 
            // comboBoxMagFilter
            // 
            this.comboBoxMagFilter.FormattingEnabled = true;
            this.comboBoxMagFilter.Location = new System.Drawing.Point(58, 30);
            this.comboBoxMagFilter.Name = "comboBoxMagFilter";
            this.comboBoxMagFilter.Size = new System.Drawing.Size(182, 21);
            this.comboBoxMagFilter.TabIndex = 1;
            this.comboBoxMagFilter.SelectedIndexChanged += new System.EventHandler(this.comboBoxMagFilter_SelectedIndexChanged);
            // 
            // comboBoxMinFilter
            // 
            this.comboBoxMinFilter.FormattingEnabled = true;
            this.comboBoxMinFilter.Location = new System.Drawing.Point(58, 3);
            this.comboBoxMinFilter.Name = "comboBoxMinFilter";
            this.comboBoxMinFilter.Size = new System.Drawing.Size(182, 21);
            this.comboBoxMinFilter.TabIndex = 0;
            this.comboBoxMinFilter.SelectedIndexChanged += new System.EventHandler(this.comboBoxMinFilter_SelectedIndexChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Lime;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.panelSettings);
            this.Controls.Add(this.pictureBoxZ1);
            this.DoubleBuffered = true;
            this.KeyPreview = true;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PictureBoxZ Test (F1-F3)";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.panelSettings.ResumeLayout(false);
            this.panelSettings.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private PictureBoxZ.PictureBoxZ pictureBoxZ1;
        private System.Windows.Forms.Panel panelSettings;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxMagFilter;
        private System.Windows.Forms.ComboBox comboBoxMinFilter;
        private System.Windows.Forms.CheckBox checkBoxMipMaps;
    }
}

