﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using PixelFormat = OpenTK.Graphics.OpenGL.PixelFormat;

namespace PictureBoxZ
{
    public static class TexFormatConverter
    {
        public static PixelFormat GetGLPixelFormat(System.Drawing.Imaging.PixelFormat PF)
        {
            switch (PF)
            {
                case System.Drawing.Imaging.PixelFormat.Indexed:
                case System.Drawing.Imaging.PixelFormat.Format1bppIndexed:
                case System.Drawing.Imaging.PixelFormat.Format4bppIndexed:
                case System.Drawing.Imaging.PixelFormat.Format8bppIndexed:
                    return PixelFormat.ColorIndex;

                case System.Drawing.Imaging.PixelFormat.Alpha:
                case System.Drawing.Imaging.PixelFormat.PAlpha:
                    return PixelFormat.Alpha;

                case System.Drawing.Imaging.PixelFormat.Format16bppGrayScale:
                    return PixelFormat.Alpha16IccSgix;
                case System.Drawing.Imaging.PixelFormat.Format16bppRgb555:
                case System.Drawing.Imaging.PixelFormat.Format16bppRgb565:
                    return PixelFormat.Bgr;
                case System.Drawing.Imaging.PixelFormat.Format16bppArgb1555:
                    return PixelFormat.Bgra;

                case System.Drawing.Imaging.PixelFormat.Format24bppRgb:
                case System.Drawing.Imaging.PixelFormat.Format48bppRgb:
                    return PixelFormat.Bgr;

                case System.Drawing.Imaging.PixelFormat.Format32bppRgb:
                case System.Drawing.Imaging.PixelFormat.Format32bppArgb:
                case System.Drawing.Imaging.PixelFormat.Format32bppPArgb:
                case System.Drawing.Imaging.PixelFormat.Format64bppArgb:
                case System.Drawing.Imaging.PixelFormat.Format64bppPArgb:
                    return PixelFormat.Bgra;

                default:
                    return PixelFormat.Bgra;
            }
        }

        public static PixelInternalFormat GetGLPixelInternalFormat(System.Drawing.Imaging.PixelFormat PF)
        {
            switch (PF)
            {
                case System.Drawing.Imaging.PixelFormat.Indexed:
                case System.Drawing.Imaging.PixelFormat.Format1bppIndexed:
                case System.Drawing.Imaging.PixelFormat.Format4bppIndexed:
                case System.Drawing.Imaging.PixelFormat.Format8bppIndexed:
                    return PixelInternalFormat.Rgba;

                case System.Drawing.Imaging.PixelFormat.Alpha:
                case System.Drawing.Imaging.PixelFormat.PAlpha:
                    return PixelInternalFormat.Alpha8;

                case System.Drawing.Imaging.PixelFormat.Format16bppGrayScale:
                    return PixelInternalFormat.Alpha16;

                case System.Drawing.Imaging.PixelFormat.Format16bppRgb555:
                    return PixelInternalFormat.Rgb5;

                case System.Drawing.Imaging.PixelFormat.Format16bppRgb565:
                    return PixelInternalFormat.R5G6B5IccSgix;

                case System.Drawing.Imaging.PixelFormat.Format16bppArgb1555:
                    return PixelInternalFormat.Rgb5A1;

                case System.Drawing.Imaging.PixelFormat.Format24bppRgb:
                    return PixelInternalFormat.Rgb8;

                case System.Drawing.Imaging.PixelFormat.Format32bppRgb:
                case System.Drawing.Imaging.PixelFormat.Format32bppArgb:
                case System.Drawing.Imaging.PixelFormat.Format32bppPArgb:
                    return PixelInternalFormat.Rgba8;

                case System.Drawing.Imaging.PixelFormat.Format48bppRgb:
                    return PixelInternalFormat.Rgb16;

                case System.Drawing.Imaging.PixelFormat.Format64bppArgb:
                case System.Drawing.Imaging.PixelFormat.Format64bppPArgb:
                    return PixelInternalFormat.Rgba16;

                default:
                    return PixelInternalFormat.Rgba;
            }
        }

        public static System.Drawing.Imaging.PixelFormat GetPixelFormat(PixelInternalFormat PIF)
        {
            switch (PIF)
            {
                case PixelInternalFormat.Alpha:
                case PixelInternalFormat.Alpha8:
                case PixelInternalFormat.R8:
                case PixelInternalFormat.R8ui:
                    return System.Drawing.Imaging.PixelFormat.Alpha;

                case PixelInternalFormat.Rgb:
                case PixelInternalFormat.Rgb8:
                case PixelInternalFormat.Rgb8ui:
                    return System.Drawing.Imaging.PixelFormat.Format24bppRgb;

                case PixelInternalFormat.Rgba:
                case PixelInternalFormat.Rgba8:
                case PixelInternalFormat.Rgba8ui:
                    return System.Drawing.Imaging.PixelFormat.Format32bppArgb;

                case PixelInternalFormat.Alpha16:
                case PixelInternalFormat.R16:
                case PixelInternalFormat.R16ui:
                    return System.Drawing.Imaging.PixelFormat.Format16bppGrayScale;

                case PixelInternalFormat.Rgb5:
                    return System.Drawing.Imaging.PixelFormat.Format16bppRgb555;

                case PixelInternalFormat.Rgb16:
                    return System.Drawing.Imaging.PixelFormat.Format48bppRgb;

                case PixelInternalFormat.Rgb5A1:
                    return System.Drawing.Imaging.PixelFormat.Format16bppArgb1555;

                case PixelInternalFormat.Rgba16:
                    return System.Drawing.Imaging.PixelFormat.Format64bppArgb;

                case PixelInternalFormat.R5G6B5IccSgix:
                    return System.Drawing.Imaging.PixelFormat.Format16bppRgb565;

                default:
                    return System.Drawing.Imaging.PixelFormat.Format32bppArgb;
            }
        }
    }
}
