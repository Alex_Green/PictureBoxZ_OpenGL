﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
//using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
//using System.Data;
//using System.Text;
using System.Diagnostics;
using System.Windows.Forms;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using PixelFormat = OpenTK.Graphics.OpenGL.PixelFormat;

namespace PictureBoxZ
{
    public partial class PictureBoxZ : GLControl
    {
        bool ControlLoaded = false;
        Bitmap ImageBMP = null;
        int TextureID = 0;
        Size ImageSize = Size.Empty;
        TextureMagFilter Image_TextureMagFilter = TextureMagFilter.Nearest;
        TextureMinFilter Image_TextureMinFilter = TextureMinFilter.Nearest;
        bool Image_MipMaps = false;

        float Stretch_Zoom = 0f;
        int Zoom_Index = 0;
        List<float> Zoom_List = new List<float>();
        Point Mouse_LocationOld = new Point();
        PointF Zoom_CenterPoint = new PointF();

        public PictureBoxZ()
        {
            InitializeComponent();
            MouseWheel += new MouseEventHandler(glControl_MouseWheel);
        }

        private void glControl_Load(object sender, EventArgs e)
        {
            if (!DesignMode)
            {
                GL.ClearColor(BackColor);
                GL.Enable(EnableCap.Texture2D);
                GL.Enable(EnableCap.Blend);
                GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
                ControlLoaded = true;

                if (ImageBMP != null)
                {
                    Image = ImageBMP;
                    ImageBMP = null;
                    MipMaps = Image_MipMaps;
                }

                glControl_Resize(sender, e);
            }
        }

        protected override void OnHandleDestroyed(EventArgs e)
        {
            base.OnHandleDestroyed(e);

            if (DesignMode || !ControlLoaded)
                return;

            if (TextureID != 0)
                GL.DeleteTexture(TextureID);
            TextureID = 0;
        }

        private void glControl_MouseEnter(object sender, EventArgs e)
        {
            if (!Focused) Focus(); //Enable focus for MouseWheel
        }

        private void glControl_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Middle)
            {
                Mouse_LocationOld = e.Location;
                Cursor = Cursors.SizeAll;
            }
            else if (e.Button == MouseButtons.XButton1)
                ZoomUpdate(false);
            else if (e.Button == MouseButtons.XButton2)
                for (int i = 0; i < ZoomList.Count; i++)
                    if (ZoomList[i] == 1.0f)
                    {
                        ZoomListIndex = i;
                        ZoomUpdate(true);
                    }
        }

        private void glControl_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Middle)
            {
                MoveImage(e.Location);
                Cursor = Cursors.Default;
            }
        }

        private void glControl_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Middle)
            {
                MoveImage(e.Location);
                Cursor = Cursors.SizeAll;
            }
        }

        private void glControl_MouseWheel(object sender, MouseEventArgs e)
        {
            if (e.Delta > 0)
            {
                if (Zoom_Index < Zoom_List.Count - 1)
                    Zoom_Index++;
            }
            else if (e.Delta < 0)
            {
                if (Zoom_Index > 0)
                    Zoom_Index--;
            }

            MoveImage(Mouse_LocationOld);
        }

        private void MoveImage(Point NewMouseCoords)
        {
            #region Zoom_CenterPoint
            Zoom_CenterPoint = new PointF(
                Zoom_CenterPoint.X - (NewMouseCoords.X - Mouse_LocationOld.X) / GetZoom,
                Zoom_CenterPoint.Y - (NewMouseCoords.Y - Mouse_LocationOld.Y) / GetZoom);
            Mouse_LocationOld = NewMouseCoords;

            float WidthZ = ClientSize.Width / GetZoom;
            float HeightZ = ClientSize.Height / GetZoom;
            float Half_WidthZ = WidthZ * 0.5f;
            float Half_HeightZ = HeightZ * 0.5f;

            if (ImageSize.Width > WidthZ)
            {
                if (Zoom_CenterPoint.X - Half_WidthZ < 0f)
                    Zoom_CenterPoint.X = Half_WidthZ;

                if (Zoom_CenterPoint.X + Half_WidthZ > ImageSize.Width)
                    Zoom_CenterPoint.X = ImageSize.Width - Half_WidthZ;
            }
            else
                Zoom_CenterPoint.X = ImageSize.Width * 0.5f;

            if (ImageSize.Height > HeightZ)
            {
                if (Zoom_CenterPoint.Y - Half_HeightZ < 0f)
                    Zoom_CenterPoint.Y = Half_HeightZ;

                if (Zoom_CenterPoint.Y + Half_HeightZ > ImageSize.Height)
                    Zoom_CenterPoint.Y = ImageSize.Height - Half_HeightZ;
            }
            else
                Zoom_CenterPoint.Y = ImageSize.Height * 0.5f;
            #endregion

            Invalidate();
        }

        #region Zoom
        [Category("Zoom")]
        public PointF ZoomCenterPoint
        {
            get
            {
                return new PointF(Zoom_CenterPoint.X / ImageSize.Width, Zoom_CenterPoint.Y / ImageSize.Height);
            }
            set
            {
                Zoom_CenterPoint.X = value.X * ImageSize.Width;
                Zoom_CenterPoint.Y = value.Y * ImageSize.Height;
                MoveImage(Mouse_LocationOld);
            }
        }

        [Category("Zoom")]
        public List<float> ZoomList
        {
            get { return Zoom_List; }
            set
            {
                Zoom_List = value;

                if (Zoom_Index > Zoom_List.Count - 1)
                    Zoom_Index = Zoom_List.Count - 1;
            }
        }

        [Category("Zoom")]
        public int ZoomListIndex
        {
            get { return Zoom_Index; }
            set
            {
                if (value < 0)
                    Zoom_Index = 0;
                else if (value > Zoom_List.Count - 1)
                    Zoom_Index = Zoom_List.Count - 1;
                else
                    Zoom_Index = value;
            }
        }

        [Category("Zoom")]
        public float GetZoom
        {
            get
            {
                if (Zoom_Index < 0 || Zoom_Index >= Zoom_List.Count)
                    return 1f;
                return Zoom_List[Zoom_Index];
            }
        }

        [Category("Zoom")]
        public float GetStretchZoom
        {
            get { return Stretch_Zoom; }
        }

        [Category("Zoom")]
        public void ZoomUpdate(bool RestoreLastZoom = false)
        {
            // Save Old Zoom!
            float LastZoom = GetZoom;

            Zoom_List.Clear();
            Zoom_Index = 0;

            // StretchZoom
            float DeltaW = ClientSize.Width / (float)ImageSize.Width;
            float DeltaH = ClientSize.Height / (float)ImageSize.Height;
            Stretch_Zoom = (DeltaW > DeltaH ? DeltaH : DeltaW);
            Zoom_List.Add(Stretch_Zoom);

            float MaxZoom = 64f;
            float MinZoom = (float)Math.Pow(2.0, Math.Ceiling(Math.Log(64.0 / Math.Max(ImageSize.Width, ImageSize.Height), 2.0)));

            // 1, 2, 4, 8 ... MaxZoom
            for (float i = 1f; i <= MaxZoom; i *= 2f)
                Zoom_List.Add(i);

            // 0.5, 0.25, 0.125 ... MinZoom
            for (float i = 0.5f; i >= MinZoom; i *= 0.5f)
                Zoom_List.Add(i);

            Zoom_List.Sort();

            int Zoom_List_Count = Zoom_List.Count - 1;
            for (int i = 0; i < Zoom_List_Count; i++)
            {
                if (Zoom_List[i] == Zoom_List[i + 1])
                {
                    Zoom_List.RemoveAt(i);
                    Zoom_List_Count--;
                    i--;
                }
            }

            if (RestoreLastZoom) // Restore Last Zoom
            {
                int MinDeltaIndex = 0;
                float MinDelta = Zoom_List[MinDeltaIndex];
                for (int i = 1; i < Zoom_List.Count; i++)
                {
                    float Delta = Math.Abs(Zoom_List[i] - LastZoom);
                    if (Delta < MinDelta)
                    {
                        MinDeltaIndex = i;
                        MinDelta = Delta;
                    }
                }
                Zoom_Index = MinDeltaIndex;
            }
            else
                Zoom_Index = Zoom_List.IndexOf(Stretch_Zoom);

            MoveImage(Mouse_LocationOld);
        }
        #endregion

        [Category("Appearance")]
        [DefaultValue(null)]
        public Image Image
        {
            get
            {
                if (DesignMode || !ControlLoaded)
                    return ImageBMP;

                if (TextureID == 0)
                    return null;

                GL.BindTexture(TextureTarget.Texture2D, TextureID);
                GL.PixelStore(PixelStoreParameter.PackAlignment, 1);

                int Texture_Width, Texture_Height, Texture_PixelInternalFormat;
                GL.GetTexLevelParameter(TextureTarget.Texture2D, 0, GetTextureParameter.TextureWidth, out Texture_Width);
                GL.GetTexLevelParameter(TextureTarget.Texture2D, 0, GetTextureParameter.TextureHeight, out Texture_Height);
                GL.GetTexLevelParameter(TextureTarget.Texture2D, 0, GetTextureParameter.TextureInternalFormat, out Texture_PixelInternalFormat);
                System.Drawing.Imaging.PixelFormat PF = TexFormatConverter.GetPixelFormat((PixelInternalFormat)Texture_PixelInternalFormat);
                PixelFormat GL_PF = TexFormatConverter.GetGLPixelFormat(PF);

                Bitmap BMP = new Bitmap(Texture_Width, Texture_Height, PF);
                BitmapData BmpData = BMP.LockBits(new Rectangle(0, 0, Texture_Width, Texture_Height), ImageLockMode.WriteOnly, BMP.PixelFormat);
                GL.GetTexImage(TextureTarget.Texture2D, 0, GL_PF, PixelType.UnsignedByte, BmpData.Scan0);
                BMP.UnlockBits(BmpData);
                BmpData = null;
                BMP.RotateFlip(RotateFlipType.RotateNoneFlipY); // Flip Y
                return BMP;
            }
            set
            {
                if (DesignMode || !ControlLoaded)
                {
                    ImageBMP = (Bitmap)value;
                    ImageSize = (value == null ? Size.Empty : value.Size);
                }
                else
                {
                    GL.BindTexture(TextureTarget.Texture2D, 0);

                    if (TextureID != 0)
                    {
                        GL.DeleteTexture(TextureID);
                        TextureID = 0;
                    }

                    if (value != null)
                    {
                        TextureID = GL.GenTexture();
                        GL.BindTexture(TextureTarget.Texture2D, TextureID);

                        Bitmap BMP = (Bitmap)value.Clone();
                        BMP.RotateFlip(RotateFlipType.RotateNoneFlipY); // Flip Y
                        ImageSize = BMP.Size;
                        Zoom_CenterPoint = new PointF(BMP.Width * 0.5f, BMP.Height * 0.5f);

                        System.Drawing.Imaging.PixelFormat PF = BMP.PixelFormat;
                        PixelFormat GL_PF = TexFormatConverter.GetGLPixelFormat(PF);
                        PixelInternalFormat GL_PIF = TexFormatConverter.GetGLPixelInternalFormat(PF);

                        BitmapData BmpData = BMP.LockBits(new Rectangle(Point.Empty, BMP.Size), ImageLockMode.ReadOnly, PF);
                        GL.TexImage2D(TextureTarget.Texture2D, 0, GL_PIF, BMP.Width, BMP.Height, 0, GL_PF, PixelType.UnsignedByte, BmpData.Scan0);
                        BMP.UnlockBits(BmpData);
                        BmpData = null;
                        BMP.Dispose();
                        BMP = null;

                        MipMaps = Image_MipMaps; // Generate Mip Maps (if true)
                    }

                    ZoomUpdate(false);
                }
            }
        }

        public Color4 GetColorAtPoint(Point MouseLocation)
        {
            if (DesignMode || !ControlLoaded)
                return new Color4(-1f, -1f, -1f, -1f);

            float[] PixelColor = new float[4]; // RGBA
            GL.ReadBuffer(ReadBufferMode.FrontAndBack);
            GL.ReadPixels(MouseLocation.X, ClientRectangle.Height - MouseLocation.Y, 1, 1, PixelFormat.Rgba, PixelType.Float, PixelColor);
            return new Color4(PixelColor[0], PixelColor[1], PixelColor[2], PixelColor[3]);
        }

        [Category("Appearance")]
        [DefaultValue(TextureMagFilter.Nearest)]
        public TextureMagFilter MagFilter
        {
            get { return Image_TextureMagFilter; }
            set
            {
                Image_TextureMagFilter = value;

                if (DesignMode || !ControlLoaded || TextureID == 0)
                    return;

                GL.BindTexture(TextureTarget.Texture2D, TextureID);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)Image_TextureMagFilter);
                MoveImage(Mouse_LocationOld);
            }
        }

        [Category("Appearance")]
        [DefaultValue(TextureMinFilter.Nearest)]
        public TextureMinFilter MinFilter
        {
            get { return Image_TextureMinFilter; }
            set
            {
                Image_TextureMinFilter = value;

                if (DesignMode || !ControlLoaded || TextureID == 0)
                    return;

                GL.BindTexture(TextureTarget.Texture2D, TextureID);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)Image_TextureMinFilter);
                MoveImage(Mouse_LocationOld);
            }
        }

        [Category("Appearance")]
        [DefaultValue(false)]
        public bool MipMaps
        {
            get { return Image_MipMaps; }
            set
            {
                Image_MipMaps = value;

                if (DesignMode || !ControlLoaded || TextureID == 0)
                    return;

                if (value)
                {
                    GL.BindTexture(TextureTarget.Texture2D, TextureID);
                    GL.GenerateMipmap(GenerateMipmapTarget.Texture2D);
                }
                else
                {
                    int Texture_Width, Texture_Height, Texture_PixelInternalFormat;
                    GL.GetTexLevelParameter(TextureTarget.Texture2D, 0, GetTextureParameter.TextureWidth, out Texture_Width);
                    GL.GetTexLevelParameter(TextureTarget.Texture2D, 0, GetTextureParameter.TextureHeight, out Texture_Height);
                    GL.GetTexLevelParameter(TextureTarget.Texture2D, 0, GetTextureParameter.TextureInternalFormat, out Texture_PixelInternalFormat);

                    int TextureID_New = GL.GenTexture();
                    int FBO = GL.GenFramebuffer();
                    GL.BindFramebuffer(FramebufferTarget.Framebuffer, FBO);
                    GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, TextureTarget.Texture2D, TextureID, 0);
                    GL.BindTexture(TextureTarget.Texture2D, TextureID_New);
                    GL.CopyTexImage2D(TextureTarget.Texture2D, 0, (PixelInternalFormat)Texture_PixelInternalFormat, 0, 0, Texture_Width, Texture_Height, 0);

                    GL.DeleteTexture(TextureID);
                    TextureID = TextureID_New;
                    GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
                    GL.DeleteFramebuffer(FBO);
                    GL.BindTexture(TextureTarget.Texture2D, TextureID);
                }

                MoveImage(Mouse_LocationOld);
            }
        }

        private void glControl_Resize(object sender, EventArgs e)
        {
            if (DesignMode || !ControlLoaded || ClientSize.Width == 0 || ClientSize.Height == 0)
                return;

            GL.Viewport(0, 0, ClientSize.Width, ClientSize.Height);

            Matrix4 Projection = Matrix4.CreateOrthographic(ClientSize.Width, ClientSize.Height, -1f, 1f);
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadMatrix(ref Projection);

            ZoomUpdate(true);
        }

        private void glControl_Paint(object sender, PaintEventArgs e)
        {
            if (DesignMode || !ControlLoaded)
                return;

            float Zoom = GetZoom;
            float Half_WidthZ = ImageSize.Width * 0.5f * Zoom;
            float Half_HeightZ = ImageSize.Height * 0.5f * Zoom;

            //Matrix4 ModelTranslation = Matrix4.CreateTranslation(0f, 0f, 0f);
            //Matrix4 ModelRotation = Matrix4.CreateRotationX(0f) * Matrix4.CreateRotationY(0f) * Matrix4.CreateRotationZ(0f);
            //Matrix4 ModelScale = Matrix4.CreateScale(1f, 1f, 1f);
            //Matrix4 Model = ModelScale * ModelRotation * ModelTranslation;
            Matrix4 Model = Matrix4.Identity;

            Matrix4 View = Matrix4.LookAt(0f, 0f, 1f, 0f, 0f, 0f, 0f, 1f, 0f);
            Matrix4 ModelView = Model * View;
            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadMatrix(ref ModelView);

            GL.Clear(ClearBufferMask.ColorBufferBit);

            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, TextureID);

            if (TextureID != 0)
            {
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)Image_TextureMagFilter);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)Image_TextureMinFilter);
                Draw(-Zoom_CenterPoint.X * Zoom + Half_WidthZ, Zoom_CenterPoint.Y * Zoom - Half_HeightZ, Half_WidthZ, Half_HeightZ);
            }
            SwapBuffers();
        }

        private void Draw(float CenterX, float CenterY, float HalfWidth, float HalfHeight)
        {
            GL.Begin(PrimitiveType.Quads);

            GL.TexCoord2(0f, 0f);
            GL.Vertex3(CenterX - HalfWidth, CenterY - HalfHeight, 0f);

            GL.TexCoord2(0f, 1f);
            GL.Vertex3(CenterX - HalfWidth, CenterY + HalfHeight, 0f);

            GL.TexCoord2(1f, 1f);
            GL.Vertex3(CenterX + HalfWidth, CenterY + HalfHeight, 0f);

            GL.TexCoord2(1f, 0f);
            GL.Vertex3(CenterX + HalfWidth, CenterY - HalfHeight, 0f);

            GL.End();
        }
    }
}