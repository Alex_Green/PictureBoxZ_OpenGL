﻿namespace PictureBoxZ
{
    partial class PictureBoxZ
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // PictureBoxZ
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.Name = "PictureBoxZ";
            this.VSync = true;
            this.Load += new System.EventHandler(this.glControl_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.glControl_Paint);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.glControl_MouseDown);
            this.MouseEnter += new System.EventHandler(this.glControl_MouseEnter);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.glControl_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.glControl_MouseUp);
            this.Resize += new System.EventHandler(this.glControl_Resize);
            this.ResumeLayout(false);

        }

        #endregion
    }
}
